package com.mindia.avisosnick.util;

public class Constants {
	
	/**
	 * ROLES DEL SISTEMA
	 */
	public static final String ROLE_USER = "ROLE_USER";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	
	/*
	 * PROVIDERS DE OAUTH
	 */
	public static final String OAUTH_PROVIDER_GOOGLE = "GOOGLE";
	
	/**
	 * CLIENTES DE OAUTH
	 */
//	public static final String GOOGLE_OAUTH_CLIENT_ID = "944140954391-iuoilfj8dkfadhsog924buo7gnt32atl.apps.googleusercontent.com";
	public static final String GOOGLE_OAUTH_CLIENT_ID = "944140954391-sjb76opctmefo2m14pt0aelqeh8cq8ub.apps.googleusercontent.com";
	
	//Regex Email
	public static final String REGEX_EMAIL = "^(.+)@(.+)$";
}
